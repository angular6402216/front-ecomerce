import { Component, OnInit  } from '@angular/core';
import { LocalStorageService } from './service/local-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'front-ecomerce';
  counter: number =  0;

  constructor(private localStorageService: LocalStorageService) { }

  ngOnInit(): void {
    this.localStorageService.getCounter().subscribe((counter: number) => {
      this.counter = counter;
    });
  }
  getCounter(){
    if(this.counter > 9){
      return "+9";
    }else{
      if(this.counter === 0)
        return "";
      else {return ""+this.counter;}
    }
  }
}
