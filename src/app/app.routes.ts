import { Routes } from '@angular/router';
import { InicioComponent } from './component/inicio/inicio.component';
import { TiendaComponent } from './component/tienda/tienda.component';
import { CategoriaComponent } from './component/categoria/categoria.component';
import { ProductoComponent } from './component/producto/producto.component';
import { CarritoComponent } from './component/carrito/carrito.component';
import { OrdenComponent } from './component/orden/orden.component';

export const routes: Routes = [
    {path: 'inicio', component: InicioComponent},
    {path: 'tienda', component: TiendaComponent},
    {path: 'categoria', component: CategoriaComponent},
    {path: 'producto', component: ProductoComponent},
    {path: 'carrito', component: CarritoComponent},
    {path: 'ordenes', component: OrdenComponent},
    {path: '', redirectTo: '/inicio', pathMatch: 'full'},
];
