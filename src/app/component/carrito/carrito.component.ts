import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTable, MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { LocalStorageService } from '../../service/local-storage.service';
import { MatDialog } from '@angular/material/dialog';
import { Product } from '../../interface/product.interface';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Router } from '@angular/router';
import { Store } from '../../interface/store.interface';
import { StoreService } from '../../service/store.service';
import { MatSelectChange, MatSelectModule } from '@angular/material/select';
import { OrderService } from '../../service/order.service';
import { Order } from '../../interface/order.interface';
import { DialogCompradoComponent } from './dialog-comprado/dialog-comprado.component';
import {MatCheckboxModule} from '@angular/material/checkbox';

@Component({
  selector: 'app-carrito',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatSlideToggleModule,
    CommonModule,
    MatDividerModule,
    MatListModule,
    MatSelectModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
  ],
  templateUrl: './carrito.component.html',
  styleUrl: './carrito.component.css',
})
export class CarritoComponent implements OnInit {
  idTienda: String = '';
  direccionEnvio: String = '';
  enviarDireccion: boolean = false;
  stores: Store[] = [];
  total: number = 0;
  order!: Order;
  displayedColumns: string[] = [
    '#',
    'description',
    'price',
    'stock',
    'subtotal',
    'acciones',
  ];

  @ViewChild(MatTable) table!: MatTable<any>;

  carrito: Product[] = [];

  constructor(
    private localStorageService: LocalStorageService,
    private storeService: StoreService,
    private orderService: OrderService,
    public dialog: MatDialog,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.carrito = this.localStorageService.getAll();
    this.actualizaTotal();
    this.getAllStores();
  }

  getAllStores() {
    this.storeService.getAll().subscribe((response: any) => {
      this.stores = response;
    });
  }
  eliminar(product: Product) {
    this.localStorageService.delete(product.id);
    this.carrito = this.localStorageService.getAll();
    if (this.carrito.length === 0) this.router.navigate(['/inicio']);
    this.actualizaTotal();
  }
  actualizaTotal() {
    this.total = 0;
    for (let i = 0; i < this.carrito.length; i++) {
      const element = this.carrito[i];
      this.total = this.total + element.price * element.stock;
    }
  }

  onCategorySelected(event: MatSelectChange) {
    const selectedStoreId = event.value;
    this.idTienda = selectedStoreId;
  }

  realizarPedido() {
    this.orderService.insert(this.getDataOrder()).subscribe((res: any) => {
      this.order = res;
      this.agregarProductosOrder();
      this.mostarDialogComprado();
    });
  }
  agregarProductosOrder() {
    for (let i = 0; i < this.carrito.length; i++) {
      const element = this.carrito[i];
      this.orderService
        .insertProduct(this.getDataProductoOrder(element))
        .subscribe((res: any) => {});
    }
  }
  getDataOrder() {
    const data = {
      name: 'Nuevo pedido',
      shippingAddres: this.direccionEnvio,
      idDelivery: this.direccionEnvio.length === 0 ? false : true,
      idStore: this.idTienda,
    };
    return data;
  }
  getDataProductoOrder(product: Product) {
    const data = {
      idOrder: this.order.id,
      idProduct: product.id,
      quantity: product.stock,
    };
    return data;
  }
  mostarDialogComprado() {
    const dialogRef = this.dialog.open(DialogCompradoComponent, {
      disableClose: true,
      minWidth: '400px', // Establece un ancho mínimo
      maxWidth: '400px', // Establece un ancho máximo
      data: { tipo: 'mensaje', campos: this.order },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.ngOnInit();
      //Tenemos que limpiar el carro y cambiar a tienda
      this.localStorageService.deleteAll();
      this.router.navigate(['/inicio']);
    });
  }
  roundToTwoDecimals(value: number): number {
    return parseFloat(value.toFixed(2));
}
}
