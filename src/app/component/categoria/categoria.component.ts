import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable, MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';

import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogTitle,
  MatDialogContent,
} from '@angular/material/dialog';
import { Category } from '../../interface/category.interface';
import { CategoryService } from '../../service/category.service';
import { DialogCategoryComponent } from './dialog-category/dialog-category.component';

@Component({
  selector: 'app-categoria',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule, 
  ],
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {
  displayedColumns: string[] = ['#','name','description', 'acciones'];
  
  @ViewChild(MatTable) table!: MatTable<any>;

  categories: Category[] = [];
  
  
  constructor(private categoryService: CategoryService,public dialog: MatDialog) {}

  ngOnInit(): void {
    this.getAll();
  }
  
  getAll() {
    this.categoryService.getAll().subscribe((response: any) => {
      this.categories = response;
    });
  }

  editar(data: any) {
    this.openModal('UPDATE', data);
  }

  eliminar(data: any) {
    this.openModal('DELETE', data);
  }
  crear(){
    this.openModal('NEW', []);
  }

  openModal(tipo: string, data: any) {
    
    const dialogRef = this.dialog.open(DialogCategoryComponent, {
      disableClose: true,
      minWidth: '400px', // Establece un ancho mínimo
      maxWidth: '400px', // Establece un ancho máximo
      data: { tipo, campos: data },
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
    
  }

}
