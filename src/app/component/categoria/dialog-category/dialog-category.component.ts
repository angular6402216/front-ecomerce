import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogTitle,
  MatDialogContent,
  MatDialogActions,
  MatDialogClose,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { Category } from '../../../interface/category.interface';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { CategoryService } from '../../../service/category.service';

export interface DialogData {
  tipo: string;
  campos: Category;
}

const data_cleam = {
  id: '',
  name: '',
  description: '',
};

@Component({
  selector: 'app-dialog-category',
  standalone: true,
  imports: [
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    MatButtonModule,
    MatIconModule,
    CommonModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
  ],
  templateUrl: './dialog-category.component.html',
  styleUrl: './dialog-category.component.css',
})
export class DialogCategoryComponent {
  datos!: Category;
  datosModificables: any;
  tipoBock = false; //Guardar Nuevo
  tipoBockM = false; //Modificar
  tipoBockE = false; //Eliminar
  tipoBockR = false; //rESTAURAR
  estado = false; //areas editables
  tipo_tramite_nombre: any;
  myForm!: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogCategoryComponent>,
    private fb: FormBuilder,
    private categoryService: CategoryService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.datosModificables = data;
  }

  ngOnInit(): void {
    switch (this.datosModificables.tipo) {
      case 'NEW':
        this.tipoBock = false;
        this.tipoBockM = false;
        this.tipoBockE = false;
        this.tipoBockR = false;
        this.estado = false;
        break;
      case 'UPDATE':
        this.tipoBock = true;
        this.tipoBockM = true;
        this.tipoBockE = false;
        this.tipoBockR = false;
        break;
      case 'DELETE':
        this.tipoBock = true;
        this.tipoBockM = false;
        this.tipoBockE = true;
        this.tipoBockR = false;
        this.estado = true;
        break;
      case 'RESTORE':
        this.tipoBock = true;
        this.tipoBockM = false;
        this.tipoBockE = false;
        this.tipoBockR = true;
        this.estado = true;
        break;
      default:
        break;
    }
    this.initForm(this.datosModificables.campos);
    this.datos = this.datosModificables.campos;
  }

  initForm(data: Category): void {
    this.myForm = this.fb.group({
      name: [{ value: data.name, disabled: this.estado }],
      description: [{ value: data.description, disabled: this.estado }],
    });
  }
  getData() {
    const data = {
      id: this.data.campos.id,
      name: this.myForm.get('name')?.value,
      description: this.myForm.get('description')?.value,
    };
    return data;
  }

  guardar() {
    this.categoryService.insert(this.getData()).subscribe((res: any) => {
      this.viewMessage(res);
    });
  }

  modificar() {
    this.categoryService.update(this.getData()).subscribe((res: any) => {
      this.viewMessage(res);
    });
  }

  eliminar() {
    this.categoryService.delete(this.datos.id).subscribe((res: any) => {
      this.viewMessage(res);
    });
  }

  viewMessage(res: any) {
    this.dialogRef.close();
  }
}
