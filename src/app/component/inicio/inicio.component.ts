import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTable, MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogTitle,
  MatDialogContent,
} from '@angular/material/dialog';
import { Product } from '../../interface/product.interface';
import { ProductService } from '../../service/product.service';
import { DialogInicioComponent } from './dialog-inicio/dialog-inicio.component';
import { LocalStorageService } from '../../service/local-storage.service';

@Component({
  selector: 'app-producto',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatSlideToggleModule,
    CommonModule,
  ],
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
})
export class InicioComponent implements OnInit {
  displayedColumns: string[] = [
    '#',
    'name',
    'description',
    'stock',
    'price',
    'acciones',
  ];

  @ViewChild(MatTable) table!: MatTable<any>;

  products: Product[] = [];
  carrito: Product[] = [];

  constructor(
    private productService: ProductService,
    private localStorageService: LocalStorageService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.carrito = this.localStorageService.getAll();
    this.getAll();
  }

  getAll() {
    this.productService.getAll().subscribe((response: Product[]) => {
      const activeProducts = response.filter((product) => product.active);
      for (let i = 0; i < activeProducts.length; i++) {
        const element = activeProducts[i];
        activeProducts[i].stock =
          activeProducts[i].stock - this.cantidadEnCarrito(element);
      }
      this.products = activeProducts;
    });
  }
  cantidadEnCarrito(product: Product): number {
    for (let i = 0; i < this.carrito.length; i++) {
      const element = this.carrito[i];
      if (this.carrito[i].id === product.id) return element.stock;
    }
    return 0;
  }

  editar(data: any) {
    this.openModal('UPDATE', data);
  }

  eliminar(data: any) {
    this.openModal('DELETE', data);
  }
  crear() {
    this.openModal('NEW', []);
  }

  openModal(tipo: string, data: any) {
    const dialogRef = this.dialog.open(DialogInicioComponent, {
      disableClose: true,
      minWidth: '300px', // Establece un ancho mínimo
      maxWidth: '300px', // Establece un ancho máximo
      data: { tipo, campos: data },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.ngOnInit();
    });
  }
}
