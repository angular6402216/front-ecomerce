import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogTitle,
  MatDialogContent,
  MatDialogActions,
  MatDialogClose,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { Category } from '../../../interface/category.interface';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { CategoryService } from '../../../service/category.service';
import { Order } from '../../../interface/order.interface';
import { OrderService } from '../../../service/order.service';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatSelectChange, MatSelectModule } from '@angular/material/select';
import { Store } from '../../../interface/store.interface';
import { StoreService } from '../../../service/store.service';

export interface DialogData {
  tipo: string;
  campos: Order;
}

const data_cleam = {
  id: '',
  name: '',
  description: '',
};

@Component({
  selector: 'app-dialog-category',
  standalone: true,
  imports: [
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    MatButtonModule,
    MatIconModule,
    CommonModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatDividerModule,
    MatListModule,
    MatSelectModule,
  ],
  templateUrl: './dialog-order.component.html',
  styleUrl: './dialog-order.component.css',
})
export class DialogOrderComponent {
  idTienda: String = '';
  direccionEnvio: String = '';
  datos!: Order;
  stores: Store[] = [];
  datosModificables: any;
  tipoBock = false; //Guardar Nuevo
  tipoBockM = false; //Modificar
  tipoBockE = false; //Eliminar
  tipoBockR = false; //rESTAURAR
  estado = false; //areas editables
  tipo_tramite_nombre: any;
  myForm!: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogOrderComponent>,
    private fb: FormBuilder,
    private orderService: OrderService,
    private storeService: StoreService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.datosModificables = data;
    this.getAllStores();
  }

  ngOnInit(): void {
    switch (this.datosModificables.tipo) {
      case 'NEW':
        this.tipoBock = false;
        this.tipoBockM = false;
        this.tipoBockE = false;
        this.tipoBockR = false;
        this.estado = false;
        break;
      case 'UPDATE':
        this.tipoBock = true;
        this.tipoBockM = true;
        this.tipoBockE = false;
        this.tipoBockR = false;
        break;
      case 'DELETE':
        this.tipoBock = true;
        this.tipoBockM = false;
        this.tipoBockE = true;
        this.tipoBockR = false;
        this.estado = true;
        break;
      case 'RESTORE':
        this.tipoBock = true;
        this.tipoBockM = false;
        this.tipoBockE = false;
        this.tipoBockR = true;
        this.estado = true;
        break;
      default:
        break;
    }
    this.initForm(this.datosModificables.campos);
    this.datos = this.datosModificables.campos;
    this.idTienda = this.datos.store.id;
    this.direccionEnvio = this.datos.shippingAddres;
  }

  getDataOrder() {
    const data = {
      id: this.datos.id,
      name: 'Nuevo pedido',
      shippingAddres: this.direccionEnvio,
      idDelivery: this.direccionEnvio.length === 0 ? false : true,
      idStore: this.idTienda,
    };
    return data;
  }
  getAllStores() {
    this.storeService.getAll().subscribe((response: any) => {
      this.stores = response;
    });
  }
  initForm(data: Category): void {
    this.myForm = this.fb.group({
      name: [{ value: data.name, disabled: this.estado }],
    });
  }
  getData() {
    const data = {
      id: this.data.campos.id,
      name: this.myForm.get('name')?.value,
      description: this.myForm.get('description')?.value,
    };
    return data;
  }

  guardar() {
    this.orderService.insert(this.getData()).subscribe((res: any) => {
      this.viewMessage(res);
    });
  }

  onCategorySelected(event: MatSelectChange) {
    // Access the selected value from the event
    const selectedStoreId = event.value;
    this.idTienda = selectedStoreId;
  }
  modificar() {
    this.orderService.update(this.getDataOrder()).subscribe((res: any) => {
      this.viewMessage(res);
    });
  }

  eliminar() {
    this.orderService.delete(this.datos.id).subscribe((res: any) => {
      this.viewMessage(res);
    });
  }

  viewMessage(res: any) {
    this.dialogRef.close();
  }
}
