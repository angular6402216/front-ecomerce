import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable, MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

import { MatDialog } from '@angular/material/dialog';
import { Order } from '../../interface/order.interface';
import { OrderService } from '../../service/order.service';
import { DialogOrderComponent } from './dialog-order/dialog-order.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categoria',
  standalone: true,
  imports: [MatTableModule, MatIconModule, MatButtonModule],
  templateUrl: './orden.component.html',
  styleUrls: ['./orden.component.css'],
})
export class OrdenComponent implements OnInit {
  displayedColumns: string[] = [
    '#',
    'name',
    'city',
    'date',
    'address',
    'state',
    'acciones',
  ];

  @ViewChild(MatTable) table!: MatTable<any>;

  orders: Order[] = [];

  constructor(
    private orderService: OrderService,
    public dialog: MatDialog,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.orderService.getAll().subscribe((response: any) => {
      this.orders = response;
    });
  }

  editar(data: any) {
    this.openModal('UPDATE', data);
  }

  eliminar(data: any) {
    this.openModal('DELETE', data);
  }
  crear() {
    this.router.navigate(['/inicio']);
  }

  openModal(tipo: string, data: any) {
    const dialogRef = this.dialog.open(DialogOrderComponent, {
      disableClose: true,
      minWidth: '400px', // Establece un ancho mínimo
      maxWidth: '400px', // Establece un ancho máximo
      data: { tipo, campos: data },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.ngOnInit();
    });
  }
}
