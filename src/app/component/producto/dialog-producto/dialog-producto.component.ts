import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogTitle,
  MatDialogContent,
  MatDialogActions,
  MatDialogClose,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { Product } from '../../../interface/product.interface';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { ProductService } from '../../../service/product.service';
import { Category } from '../../../interface/category.interface';
import { CategoryService } from '../../../service/category.service';

export interface DialogData {
  tipo: string;
  campos: Product;
}

const data_cleam = {
  id: '',
  name: '',
  description: '',
  stock: 0,
  price: 0,
  active: false,
  idCategory: '',
};

@Component({
  selector: 'app-dialog-producto',
  standalone: true,
  imports: [
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    MatButtonModule,
    MatIconModule,
    CommonModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatSelectModule,
  ],
  templateUrl: './dialog-producto.component.html',
  styleUrl: './dialog-producto.component.css',
})
export class DialogProductoComponent {
  datos!: Product;
  categories: Category[] = [];
  datosModificables: any;
  tipoBock = false; //Guardar Nuevo
  tipoBockM = false; //Modificar
  tipoBockE = false; //Eliminar
  tipoBockR = false; //rESTAURAR
  estado = false; //areas editables
  tipo_tramite_nombre: any;
  myForm!: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogProductoComponent>,
    private fb: FormBuilder,
    private productService: ProductService,
    private categoryService: CategoryService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.datosModificables = data;
    this.getAllCategories();
  }

  getAllCategories() {
    this.categoryService.getAll().subscribe((response: any) => {
      this.categories = response;
    });
  }
  ngOnInit(): void {
    switch (this.datosModificables.tipo) {
      case 'NEW':
        this.tipoBock = false;
        this.tipoBockM = false;
        this.tipoBockE = false;
        this.tipoBockR = false;
        this.estado = false;
        break;
      case 'UPDATE':
        this.tipoBock = true;
        this.tipoBockM = true;
        this.tipoBockE = false;
        this.tipoBockR = false;
        break;
      case 'DELETE':
        this.tipoBock = true;
        this.tipoBockM = false;
        this.tipoBockE = true;
        this.tipoBockR = false;
        this.estado = true;
        break;
      case 'RESTORE':
        this.tipoBock = true;
        this.tipoBockM = false;
        this.tipoBockE = false;
        this.tipoBockR = true;
        this.estado = true;
        break;
      default:
        break;
    }
    this.initForm(this.datosModificables.campos);
    this.datos = this.datosModificables.campos;
  }

  initForm(data: Product): void {
    this.myForm = this.fb.group({
      name: [{ value: data.name, disabled: this.estado }],
      description: [{ value: data.description, disabled: this.estado }],
      stock: [{ value: data.stock, disabled: this.estado }],
      price: [{ value: data.price, disabled: this.estado }],
      active: [{ value: data.active, disabled: this.estado }],
      idCategory: [{ value: data.idCategory, disabled: this.estado }],
    });
  }
  getData() {
    const data = {
      id: this.data.campos.id,
      name: this.myForm.get('name')?.value,
      description: this.myForm.get('description')?.value,
      stock: this.myForm.get('stock')?.value,
      price: this.myForm.get('price')?.value,
      active: this.myForm.get('active')?.value,
      idCategory: '5816bbc8-bf2d-4ac1-b126-d439cbfaee58', //this.myForm.get('idCategory')?.value,
    };
    return data;
  }

  guardar() {
    this.productService.insert(this.getData()).subscribe((res: any) => {
      this.viewMessage(res);
    });
  }

  modificar() {
    this.productService.update(this.getData()).subscribe((res: any) => {
      this.viewMessage(res);
    });
  }

  eliminar() {
    this.productService.delete(this.datos.id).subscribe((res: any) => {
      this.viewMessage(res);
    });
  }

  viewMessage(res: any) {
    this.dialogRef.close();
  }
}
