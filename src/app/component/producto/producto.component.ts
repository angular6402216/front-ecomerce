import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTable, MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {
  MatSlideToggleModule,
  _MatSlideToggleRequiredValidatorModule,
} from '@angular/material/slide-toggle';

import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogTitle,
  MatDialogContent,
} from '@angular/material/dialog';
import { Product } from '../../interface/product.interface';
import { ProductService } from '../../service/product.service';
import { DialogProductoComponent } from './dialog-producto/dialog-producto.component';

@Component({
  selector: 'app-producto',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatSlideToggleModule, 
    _MatSlideToggleRequiredValidatorModule,
    CommonModule,
  ],
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {
  displayedColumns: string[] = ['#','name','description','stock','price','active', 'acciones'];
  
  @ViewChild(MatTable) table!: MatTable<any>;

  products: Product[] = [];
  
  
  constructor(private productService: ProductService,public dialog: MatDialog) {}

  ngOnInit(): void {
    this.getAll();
  }
  
  getAll() {
    this.productService.getAll().subscribe((response: any) => {
      this.products = response;
    });
  }

  editar(data: any) {
    this.openModal('UPDATE', data);
  }

  eliminar(data: any) {
    this.openModal('DELETE', data);
  }
  crear(){
    this.openModal('NEW', []);
  }

  openModal(tipo: string, data: any) {
    
    const dialogRef = this.dialog.open(DialogProductoComponent, {
      disableClose: true,
      minWidth: '400px', // Establece un ancho mínimo
      maxWidth: '400px', // Establece un ancho máximo
      data: { tipo, campos: data },
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  onToggleChange(product:Product, event: any) {
    // Aquí puedes ejecutar cualquier lógica adicional
    product.active = event.checked;

    this.productService.update(this.getData(product)).subscribe((response: any) => {
      console.log("Actualizado");
    });
    //guardamos y actualizamos 

  }

  getData(product:Product) {
    const data = {
      id: product.id,
      name: product.name,
      description: product.description,
      stock: product.stock,
      price: product.price,
      active: product.active,
      idCategory: product.idCategory, //this.myForm.get('idCategory')?.value,
    };
    return data;
  }

}
