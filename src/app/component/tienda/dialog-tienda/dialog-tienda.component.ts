import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogTitle,
  MatDialogContent,
  MatDialogActions,
  MatDialogClose,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { Store } from '../../../interface/store.interface';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { StoreService } from '../../../service/store.service';

export interface DialogData {
  tipo: string;
  campos: Store;
}

const data_cleam = {
  id: '',
  name: '',
  address: '',
  city: '',
  openingHours: '',
};

@Component({
  selector: 'app-dialog-tienda',
  standalone: true,
  imports: [
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    MatButtonModule,
    MatIconModule,
    CommonModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
  ],
  templateUrl: './dialog-tienda.component.html',
  styleUrl: './dialog-tienda.component.css',
})
export class DialogTiendaComponent {
  datos!: Store;
  datosModificables: any;
  tipoBock = false; //Guardar Nuevo
  tipoBockM = false; //Modificar
  tipoBockE = false; //Eliminar
  tipoBockR = false; //rESTAURAR
  estado = false; //areas editables
  tipo_tramite_nombre: any;
  myForm!: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogTiendaComponent>,
    private fb: FormBuilder,
    private storeService: StoreService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.datosModificables = data;
  }

  ngOnInit(): void {
    switch (this.datosModificables.tipo) {
      case 'NEW':
        this.tipoBock = false;
        this.tipoBockM = false;
        this.tipoBockE = false;
        this.tipoBockR = false;
        this.estado = false;
        break;
      case 'UPDATE':
        this.tipoBock = true;
        this.tipoBockM = true;
        this.tipoBockE = false;
        this.tipoBockR = false;
        break;
      case 'DELETE':
        this.tipoBock = true;
        this.tipoBockM = false;
        this.tipoBockE = true;
        this.tipoBockR = false;
        this.estado = true;
        break;
      case 'RESTORE':
        this.tipoBock = true;
        this.tipoBockM = false;
        this.tipoBockE = false;
        this.tipoBockR = true;
        this.estado = true;
        break;
      default:
        break;
    }
    this.initForm(this.datosModificables.campos);
    this.datos = this.datosModificables.campos;
  }

  initForm(data: Store): void {
    this.myForm = this.fb.group({
      name: [{ value: data.name, disabled: this.estado }],
      address: [{ value: data.address, disabled: this.estado }],
      city: [{ value: data.city, disabled: this.estado }],
      openingHours: [{ value: data.openingHours, disabled: this.estado }],
    });
  }
  getData() {
    const data = {
      id: this.data.campos.id,
      name: this.myForm.get('name')?.value,
      address: this.myForm.get('address')?.value,
      city: this.myForm.get('city')?.value,
      openingHours: this.myForm.get('openingHours')?.value,
    };
    return data;
  }

  guardar() {
    this.storeService.insert(this.getData()).subscribe((res: any) => {
      this.viewMessage(res);
    });
  }

  modificar() {
    this.storeService.update(this.getData()).subscribe((res: any) => {
      this.viewMessage(res);
    });
  }

  eliminar() {
    this.storeService.delete(this.datos.id).subscribe((res: any) => {
      this.viewMessage(res);
    });
  }

  viewMessage(res: any) {
    this.dialogRef.close();
  }
}
