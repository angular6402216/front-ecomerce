import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable, MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';

import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogTitle,
  MatDialogContent,
} from '@angular/material/dialog';
import { Store } from '../../interface/store.interface';
import { StoreService } from '../../service/store.service';
import { DialogTiendaComponent } from './dialog-tienda/dialog-tienda.component';

@Component({
  selector: 'app-tienda',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule, 
  ],
  templateUrl: './tienda.component.html',
  styleUrls: ['./tienda.component.css']
})
export class TiendaComponent implements OnInit {
  displayedColumns: string[] = ['#','name','address', 'ciudad','atencion', 'acciones'];
  
  @ViewChild(MatTable) table!: MatTable<any>;

  stores: Store[] = [];
  
  
  constructor(private storeService: StoreService,public dialog: MatDialog) {}

  ngOnInit(): void {
    this.getAll();
  }
  
  getAll() {
    this.storeService.getAll().subscribe((response: any) => {
      this.stores = response;
    });
  }

  editar(data: any) {
    this.openModal('UPDATE', data);
  }

  eliminar(data: any) {
    this.openModal('DELETE', data);
  }
  crear(){
    this.openModal('NEW', []);
  }

  openModal(tipo: string, data: any) {
    
    const dialogRef = this.dialog.open(DialogTiendaComponent, {
      disableClose: true,
      minWidth: '400px', // Establece un ancho mínimo
      maxWidth: '400px', // Establece un ancho máximo
      data: { tipo, campos: data },
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

}
