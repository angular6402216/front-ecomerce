import { Store } from './store.interface';

export interface Order {
  id: string;
  name: string;
  date: string;
  shippingAddres: string;
  isDelivery: boolean;
  store: Store;
  isDeleted: boolean;
}
