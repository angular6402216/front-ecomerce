import { Category } from "./category.interface";

export interface Product {
    id: string;
    name: string;
    description: string;
    stock: number;
    price: number;
    active: boolean;
    idCategory: String;
  }