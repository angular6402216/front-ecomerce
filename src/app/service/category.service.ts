import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Category } from '../interface/category.interface';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private URL_API = 'http://localhost:8080/category';

  constructor(private http: HttpClient) { }

  getAll(): Observable<Category[]> {
    return this.http.get<Category[]>(this.URL_API);
  }

  getById(id: string): Observable<Category> {
    const url = `${this.URL_API}/${id}`;
    return this.http.get<Category>(url);
  }

  insert(data: any): Observable<Category> {
    return this.http.post<Category>(this.URL_API, data);
  }

  update(data: any): Observable<Category> {
    const url = `${this.URL_API}/${data.id}`;
    return this.http.put<Category>(url, data);
  }

  delete(id: string): Observable<void> {
    const url = `${this.URL_API}/${id}`;
    return this.http.delete<void>(url);
  }  
}
