import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Order } from '../interface/order.interface';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private URL_API = 'http://localhost:8080/order';
  constructor(private http: HttpClient) { }

  getAll(): Observable<Order[]> {
    return this.http.get<Order[]>(this.URL_API, httpOptions);
  }

  insert(data: any): Observable<Order> {
    return this.http.post<any>(this.URL_API , data, httpOptions);
  }
  update(data: any): Observable<Order> {
    const url = `${this.URL_API}/${data.id}`;
    return this.http.put<Order>(url, data, httpOptions);
  }
  
  insertProduct(data: any): Observable<void> {
    return this.http.post<any>(this.URL_API+"/insert_product" , data, httpOptions);
  }

  delete(id: string): Observable<void> {
    const url = `${this.URL_API}/${id}`;
    return this.http.delete<void>(url, httpOptions);
  } 
}
