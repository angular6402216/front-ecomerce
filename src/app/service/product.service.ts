import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../interface/product.interface';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private URL_API = 'http://localhost:8080/product';

  constructor(private http: HttpClient) { }

  getAll(): Observable<Product[]> {
    return this.http.get<Product[]>(this.URL_API, httpOptions);
  }

  getById(id: string): Observable<Product> {
    const url = `${this.URL_API}/${id}`;
    return this.http.get<Product>(url, httpOptions);
  }

  insert(data: any): Observable<Product> {
    return this.http.post<any>(this.URL_API , data, httpOptions);
  }

  update(data: any): Observable<Product> {
    const url = `${this.URL_API}/${data.id}`;
    return this.http.put<Product>(url, data, httpOptions);
  }

  delete(id: string): Observable<void> {
    const url = `${this.URL_API}/${id}`;
    return this.http.delete<void>(url, httpOptions);
  }  
}
