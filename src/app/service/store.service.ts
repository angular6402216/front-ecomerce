import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Store } from '../interface/store.interface';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  private URL_API = 'http://localhost:8080/store';

  constructor(private http: HttpClient) { }

  getAll(): Observable<Store[]> {
    return this.http.get<Store[]>(this.URL_API, httpOptions);
  }

  getById(id: string): Observable<Store> {
    const url = `${this.URL_API}/${id}`;
    return this.http.get<Store>(url, httpOptions);
  }

  insert(data: any): Observable<Store> {
    return this.http.post<any>(this.URL_API , data, httpOptions);
  }

  update(data: any): Observable<Store> {
    const url = `${this.URL_API}/${data.id}`;
    return this.http.put<Store>(url, data, httpOptions);
  }

  delete(id: string): Observable<void> {
    const url = `${this.URL_API}/${id}`;
    return this.http.delete<void>(url, httpOptions);
  }  
}
